#include <stdio.h>

#define SIZE 200000
struct stack {long long pos; int stack[SIZE];};

void stack_push(struct stack *p, const int *item)
{
	p->stack[p->pos] = *item;
	++p->pos;
}

void stack_pull(int *item, struct stack *p)
{
	--p->pos;
	*item = p->stack[p->pos];
}

int stack_full(long long *pos)
{
	return *pos != SIZE - 1 ? 0 : 1;
}

int stack_empty(long long *pos)
{
	return !(*pos);
}
