CFLAGS = -Wall -O2
CC = gcc
fib: main.c stack.h
	$(CC) main.c -o fib $(CFLAGS)

clean:
	-rm -f fib
